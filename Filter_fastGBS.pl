#!/usr/bin/env perl

# Permet de trier et simplifier un fichier .vcf issu du programme fastGBS :
# - une seule base REF
# - une seule base ALT
# - GQ = qualité (phred score)
# - NR = nombre de reads couvrant un variant (équivalent au DP de samtools)
# - NV = nombre de reads contenant un variant (équivalent à un DPalt)
# - simplification des noms de génotypes et des call

# Auteur : Sophie Blanc
# Date : 16/10/2017

use strict;
use warnings;
use Getopt::Long;
use FileHandle;

my $input;
my $nr=4;
my $vcf;


my $result = &GetOptions("input|i=s" => \$input,
			 "read_depth|nr=i" => \$nr);

unless (defined($input))
	{
	print STDERR sprintf("\n");
	print STDERR sprintf("Usage: Filter_fastGBS.pl --input|-i <vcf file> --read_depth|-nr <minimal read depth per genotype, default = 4> \n\n");
	exit;
}

$vcf = new FileHandle();
open ($vcf, $input);

my $gq; # genotype quality
my $nr_gen; # number of reads covering variant
my $nv; # number of reads containing variant
my $line;
my $call;
my @genotypes;
my @cases;
my @element;
my @AD;

while(my $line = <$vcf>){
	chomp($line);
	my @cases = split("\t", $line);
	if ($line =~/^##/){ # Supprime les commentaires situés dans le header
	next;
	}elsif ($line =~/^#[^#]/){ # Pour la ligne des noms de génotypes
		print (join("\t", @cases[0..8])); # Imprime les 8 1ères colonnes
		for (my $i=9; $i<=$#cases; $i+=1){ # Pour toutes les colonnes (noms de génotypes)
			$cases[$i] =~ /^.+_\d_(.+)\.sort$/;
			my $name = $1;
			print "\t$name"; # Imprime le nom exact des génotypes
		}
		print "\n";
	} else { # Pour toutes les autres lignes du fichiers (variants)
		
		@genotypes = @cases[9..$#cases];
		if ($cases[3] =~ /^[A-Z]?$/ && $cases[4] =~ /^[A-Z]?$/ && $cases[5] == 2965 && $cases[6] =~ /PASS/){ # Ne retient que les variants ayant un seul allèle REf, un seul allèle ALT, dont la qualité globale est égale à 2965 (maximum possible), et qui indiquent "PASS" dans la colonne filter
		
			print (join("\t", @cases[0..8])); # Imprime les 8 1ères colonnes
			#print (join("_", @genotypes));
			
			foreach my $i (@genotypes){ # Pour chaque génotype
				@element = split(":", $i);
				$call = $element[0];
				
				if ($call =~ /1\/0/){ # au cas où on ait des 1/0, rend ce call équivalent aux 0/1
					$call = "0\/1";
				}
				
				$gq = $element[3];
				$nr_gen = $element[4];
				$nv = $element[5];
								
				if ($call eq "0/1" && $gq >= 20 && $nr_gen >= $nr && $nv >= 2){
					print "\t", $call;
				} elsif ($call eq "0/0" && $gq >= 20 && $nr_gen >= $nr && $nv >= 0){
					print "\t", $call;
				} elsif ($call eq "1/1" && $gq >= 20 && $nr_gen >= $nr && $nv >= 4){
					print "\t", $call;
				} else {
					print "\t./.";
				}
			}
			print "\n";
		}
	}
}

close $vcf;
exit;