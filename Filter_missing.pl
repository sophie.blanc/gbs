#!/usr/bin/env perl

# Permet de trier et simplifier un fichier .txt issu du filtre Filter_fastGBS :
# pas plus de tant de données manquantes par SNP (20% par défaut)

# Auteur : Sophie Blanc
# Date : 23/10/2017

use strict;
use warnings;
use Getopt::Long;
use FileHandle;

my $input;
my $max_missing=20;
my $txt;


my $result = &GetOptions("input|i=s" => \$input,
			 "max_missing|mm=i" => \$max_missing);

unless (defined($input))
	{
	print STDERR sprintf("\n");
	print STDERR sprintf("Usage: Filter_missing.pl --input|-i <txt file> --max_missing|-mm <maximal percentage of missing data allowed per variant, default = 20> \n\n");
	exit;
}

$txt = new FileHandle();
open ($txt, $input);

my $missing;
my $seuil;
my $line;
my @genotypes;
my @cases;

while(my $line = <$txt>){
	chomp($line);
	@cases = split("\t", $line);
	@genotypes = @cases[9..$#cases];
	
	$missing=0;
	for (my$i=0; $i<= $#genotypes; $i++){
		if ($genotypes[$i] eq "./."){
			$missing++;
		}
	}
	
	$seuil = ($max_missing*@genotypes)/100;
	
	if ($missing <= $seuil){
		print "$line\n";
	} else {
		next;
	}
}

close $txt;
exit;